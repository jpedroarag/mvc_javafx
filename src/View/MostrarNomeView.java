package View;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import Model.Pessoa;

public class MostrarNomeView {
    
    private Button voltarButton;
    private Label nomeLabel;
    private Scene root;
    
    private Pessoa model;
    
    public MostrarNomeView(Pessoa pessoa) {
        this.model = pessoa;

        Font fonte = new Font("Segoe UI Light", 20);
        nomeLabel = new Label("Olá " + this.model.getNome() + "!");
        nomeLabel.setFont(fonte);

        voltarButton = new Button("Voltar");

        VBox vbox = new VBox(nomeLabel, voltarButton);
        vbox.setPadding(new Insets(16,16,16,16));
        vbox.setSpacing(8);
        vbox.setAlignment(Pos.CENTER);

        root = new Scene(vbox, 300, 150);
    }
    
    public void show(Stage primary) {
        nomeLabel.setText("Olá " + this.model.getNome() + "!");
        primary.setScene(root);
    }

    public Button getVoltarButton() {
        return this.voltarButton;
    }

    public void setPessoa(Pessoa pessoa) {
        this.model = pessoa;
    }

}
