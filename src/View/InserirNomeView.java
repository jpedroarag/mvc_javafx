/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import Model.Pessoa;

/**
 *
 * @author Alunos
 */
public class InserirNomeView {

    private TextField nomeField;
    private Button mostrarButton;
    private Scene root;

    private Pessoa model;
    
    public InserirNomeView(Pessoa pessoa) {
        this.model = pessoa;

        Font fonte = new Font("Segoe UI Light", 20);
        Label nomeLabel = new Label("Digite seu nome");
        nomeLabel.setFont(fonte);

        fonte = new Font("Segoe UI Light", 14);
        nomeField = new TextField();
        nomeField.setPromptText("Seu nome aqui");
        nomeField.setFont(fonte);
        nomeField.setAlignment(Pos.CENTER);

        mostrarButton = new Button("Ver mensagem");

        VBox vbox = new VBox(nomeLabel, nomeField, mostrarButton);
        vbox.setPadding(new Insets(16,16,16,16));
        vbox.setSpacing(8);
        vbox.setAlignment(Pos.CENTER);

        root = new Scene(vbox, 300, 150);
    }
    
    public void show(Stage primary) {
        primary.setScene(root);
    }

    public Button getMostrarButton() {
        return this.mostrarButton;
    }

    public TextField getNomeField() {
        return this.nomeField;
    }

}
