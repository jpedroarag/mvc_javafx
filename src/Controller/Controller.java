package Controller;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.Stage;
import Model.Pessoa;
import View.InserirNomeView;
import View.MostrarNomeView;

public class Controller implements EventHandler<ActionEvent> {

    private final InserirNomeView inserirNomeView;
    private MostrarNomeView mostrarNomeView;
    private Stage window;

    private final Pessoa alguem;

    public Controller(Pessoa pessoa, InserirNomeView inserirNomeView) {
        this.alguem = pessoa;
        this.inserirNomeView = inserirNomeView;
    }

    public void setWindow(Stage window) {
        this.window = window;
    }

    @Override
    public void handle(ActionEvent event) {
        if(event.getSource() == inserirNomeView.getMostrarButton()) {
            String nome = inserirNomeView.getNomeField().getText();
            alguem.setNome(nome);

            if (mostrarNomeView == null) {
                mostrarNomeView = new MostrarNomeView(alguem);
                mostrarNomeView.getVoltarButton().setOnAction(this);
            } else {
                mostrarNomeView.setPessoa(alguem);
            }

            mostrarNomeView.show(window);

        }

        if(event.getSource() == mostrarNomeView.getVoltarButton()) {
            inserirNomeView.show(window);
        }
    }
    
}
