import Controller.Controller;
import Model.Pessoa;
import View.InserirNomeView;
import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application {

    public static void main(String[] args) {
        Main.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Pessoa pessoa = new Pessoa("JP");
        InserirNomeView inserirNomeView = new InserirNomeView(pessoa);
        Controller controller = new Controller(pessoa, inserirNomeView);

        controller.setWindow(primaryStage);
        inserirNomeView.getMostrarButton().setOnAction(controller);
        inserirNomeView.show(primaryStage);
        primaryStage.show();
    }
}
